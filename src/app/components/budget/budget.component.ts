import { Component, Input, Output, EventEmitter } from '@angular/core';
import Budget from '../budget';
import Expense from '../Expense';

@Component({
  selector: 'app-budget',
  templateUrl: './budget.component.html',
  styleUrls: ['./budget.component.css']
})
export class BudgetComponent {
  @Input() all_expenses: Expense[] = [];
  @Input() budget!: Budget;
  @Output() trigger = new EventEmitter();

  editMode = false;
  editModeDivisa = false;
}
