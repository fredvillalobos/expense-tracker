export default interface Expense {
  id?: number;
  type: string;
  amount: string;
  name: string;
}
