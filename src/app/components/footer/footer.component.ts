import { Component, Input } from '@angular/core';
import Budget from '../budget';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})

export class FooterComponent {
  @Input() budget: Budget = { budget: '0' };
  @Input() expenses: number = 0;
  @Input() entries: number = 0;
  @Input() balance: number = 0;
}
