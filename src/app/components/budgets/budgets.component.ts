import { Component, Input, Output, EventEmitter } from '@angular/core';
import { TaskService } from 'src/app/services/task.service';
import Expense from '../Expense';
import { Type } from '../Type';

@Component({
  selector: 'app-budgets',
  templateUrl: './budgets.component.html',
  styleUrls: ['./budgets.component.css']
})

export class BudgetsComponent {
  @Input() all_expenses: Expense[] = [];
  @Input() item: Expense = {
    type: '',
    amount: '',
    name: ''
  };
  @Output() trigger = new EventEmitter();

  editMode = false;
  updateMode = false;
  typesList: string[] = [Type.Ingreso, Type.Gasto];
  selectedType: string = Type.Gasto;

  constructor(private taskService: TaskService) { }

  delete_task(task: Expense) {
    this.taskService.delete_item(task).subscribe(() => {
      this.all_expenses = this.all_expenses.filter((t) => t.id !== task.id);
      this.trigger.emit();
    });
  }

  update_task(task: Expense) {
    this.taskService.update_item(task).subscribe(() => {
      this.trigger.emit();
    });
  }

  add_task(name: string, type: string, amount: string) {
    if (!name) { return; }
    if (parseInt(amount) < 0) { return; }
    let task: Expense = { name, type, amount };
    this.taskService.add_item(task).subscribe((new_task) => {
      this.all_expenses.push(new_task);
      this.trigger.emit();
    });
  }

}
