export default interface ItemType {
  id?: number;
  type: string;
  blocked: boolean;
  name: string;
}
