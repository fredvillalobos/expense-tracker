import { Component, Input, Output, EventEmitter } from '@angular/core';
import ItemType from '../ItemType';

@Component({
  selector: 'app-divisa',
  templateUrl: './divisa.component.html',
  styleUrls: ['./divisa.component.css']
})

export class DivisaComponent {
  @Input() item!: ItemType;
  @Output() update = new EventEmitter<ItemType>();
  @Output() remove = new EventEmitter<ItemType>();
  // editMode = false;
}
