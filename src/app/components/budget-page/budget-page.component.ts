import { Component, OnInit } from '@angular/core';
import Budget from '../budget';
import Expense from '../Expense';

import { TaskService } from 'src/app/services/task.service';
import { Type } from '../Type';

@Component({
  selector: 'app-budget-page',
  templateUrl: './budget-page.component.html',
  styleUrls: ['./budget-page.component.css']
})
export class BudgetPageComponent implements OnInit {

  all_expenses: Expense[] = [];

  budget: Budget = { budget: '1000' };
  expenses: number = 0;
  entries: number = 0;
  balance: number = 0;

  constructor(private taskService: TaskService) { }

  changeBudget(budget: Budget) {
    console.log(budget);
  }

  ngOnInit(): void {
    this.calculate();
  }

  calculate() {
    this.taskService.get_items().subscribe((all_expenses) => {
      this.all_expenses = all_expenses;

      this.entries = this.all_expenses.filter(item => { return item.type === Type.Ingreso; }).map(item => {
        return parseInt(item.amount);
      }).reduce((a, b) => a + b, 0);

      this.expenses = this.all_expenses.filter(item => { return item.type === Type.Gasto; }).map(item => {
        return parseInt(item.amount);
      }).reduce((a, b) => a + b, 0);

      this.balance = (parseInt(this.budget.budget) + this.entries) - this.expenses;

      console.log(this.budget.budget);
      console.log(this.all_expenses);
      console.log(this.expenses);
      console.log(this.entries);
    });
  }
}
