import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/services/task.service';
import ItemType from '../ItemType';

@Component({
  selector: 'app-divisas',
  templateUrl: './divisas.component.html',
  styleUrls: ['./divisas.component.css']
})

export class DivisasComponent implements OnInit {
  item_types: ItemType[] = [];
  editMode = false;

  constructor(private taskService: TaskService) { }

  ngOnInit(): void {
    this.get_tasks();
  }

  get_tasks() {
    this.taskService.get_item_types().subscribe((item_types) => {
      this.item_types = item_types;
      console.log(this.item_types);
    });
  }

  delete_task(task: ItemType) {
    if (task.blocked) { return; }
    this.taskService.delete_item_types(task).subscribe(() => {
      this.item_types = this.item_types.filter((t) => t.id !== task.id);
    });
  }

  update_task(task: ItemType) {
    if (!task.name) return;
    if (!task.type) return;
    if (task.blocked) { return; }
    this.taskService.update_item_types(task).subscribe(() => {
      this.get_tasks();
    });
  }

  add_task(name: string, type: string) {
    if (!name) return;
    if (!type) return;
    let task: ItemType = { name, blocked: false, type };
    this.taskService.add_item_types(task).subscribe((new_task) => {
      this.item_types.push(new_task);
    });
  }

}
