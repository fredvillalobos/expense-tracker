import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import ItemType from '../components/ItemType';
import Expense from '../components/Expense';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root'
})

export class TaskService {
  private api_url = 'http://localhost:5000/';

  constructor(private http: HttpClient) { }

  get_item_types(): Observable<ItemType[]> {
    return this.http.get<ItemType[]>(this.api_url + 'item_types');
  }

  delete_item_types(item: ItemType): Observable<ItemType> {
    const url = `${this.api_url + 'item_types'}/${item.id}`;
    return this.http.delete<ItemType>(url);
  }

  update_item_types(item: ItemType): Observable<ItemType> {
    const url = `${this.api_url + 'item_types'}/${item.id}`;
    return this.http.put<ItemType>(url, item, httpOptions);
  }

  add_item_types(item: ItemType): Observable<ItemType> {
    return this.http.post<ItemType>(this.api_url + 'item_types', item, httpOptions);
  }

  get_items(): Observable<Expense[]> {
    return this.http.get<Expense[]>(this.api_url + 'all_expenses');
  }

  delete_item(item: Expense): Observable<Expense> {
    const url = `${this.api_url + 'all_expenses'}/${item.id}`;
    return this.http.delete<Expense>(url);
  }

  update_item(item: Expense): Observable<Expense> {
    const url = `${this.api_url + 'all_expenses'}/${item.id}`;
    return this.http.put<Expense>(url, item, httpOptions);
  }

  add_item(item: Expense): Observable<Expense> {
    return this.http.post<Expense>(this.api_url + 'all_expenses', item, httpOptions);
  }
}
