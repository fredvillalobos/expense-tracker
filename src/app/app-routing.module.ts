
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BudgetPageComponent } from './components/budget-page/budget-page.component';

const routes: Routes = [
  { path: '', redirectTo: '/budget', pathMatch: 'full' },
  { path: "budget", component: BudgetPageComponent },
 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

