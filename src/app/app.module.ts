import { NgModule, isDevMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


import { BudgetComponent } from './components/budget/budget.component';
import { BudgetsComponent } from './components/budgets/budgets.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { BudgetPageComponent } from './components/budget-page/budget-page.component';
import { DivisasComponent } from './components/divisas/divisas.component';
import { DivisaComponent } from './components/divisa/divisa.component';

@NgModule({
  declarations: [
    AppComponent,
    BudgetComponent,
    BudgetsComponent,
    FooterComponent,
    HeaderComponent,
    BudgetPageComponent,
    DivisasComponent,
    DivisaComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: !isDevMode(),
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
